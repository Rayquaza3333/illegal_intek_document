def play_melody(melody, sound_basedir):
    """
    :param melody: A collection (iterable) of notes, named after the international pitch notation, of the melody to play.
    :param sound_basedir: A string representing the path of the directory containing the note sound files. The function play_melody uses this parameter to find and load each note sound file.
    :return: Play the melody
    """
    # PLay the melody:

    import pygame
    pygame.init()

    notes = ['a','bb','b','c','db','d','eb','e','f','gb','g','ab']
    change_notes = {"a#": "bb", "c#": "db", "d#": "eb", "f#": "gb"} # Without the g# case"

    for i in melody:

        # Catch Errors and change note name

        ii = i
        i = i.lower()

        for index in range(len(i)-1):
            if i[index] not in "abcdefgb#":
                raise TypeError('{} is not a sound note'.format(ii))

        if i[0:2] in change_notes.keys():

            if i == 'g#':
                i[0:2] = 'ab'
                i = i[0:2] + str(int(i[2]) +1)

            else:
                i = change_notes[i[0:2]] +i[2]


        # Play sound

        sound_dir = sound_basedir + i +'.ogg'

        print(sound_dir)
        sound = pygame.mixer.Sound(sound_dir)

        sound.play(maxtime = 400, fade_ms = 100)

        pygame.time.delay(400)

        pygame.mixer.fadeout(100)

import os
dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
dir = os.path.join(dir,'sound','piano',"")
sound = ['d3',"d4","d4"]

# /home/pqthoang/Devel/pqthoang/intek-sm-python_basics-master/sounds/piano/
play_melody(sound, dir)
